const doImport = require('./import')

const pluginEntry = broker => {
  broker.addManuscriptImporter(
    'any', // 'manual', 'automatic' or 'any'
    doImport(
      broker.findManuscriptWithUri,
      broker.findManuscriptWithDoi,
      broker.getStubManuscriptObject,
      broker.logger,
    ),
  )
}

module.exports = pluginEntry
